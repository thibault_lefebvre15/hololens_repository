﻿using System.Collections.Generic;
using HoloToolkit.Unity;
using UnityEngine;

public class ObjectManager : Singleton<ObjectManager>
{
    [Tooltip("Will be calculated at runtime if is not preset.")]
    public float ScaleFactor = 1.0f;    //Scale factor to apply to the maze

    public void PlaceMaze(Vector3 positionCenter, Vector3 rotation) //Function to place the maze in the scene
    {
        CreateMazeFromCSV.Instance.CreateMaze();
        GameObject maze = GameObject.Find("Maze");
        var position = positionCenter - new Vector3(CreateMazeFromCSV.Instance.MazeSize.x* .5f, 0, CreateMazeFromCSV.Instance.MazeSize.z* .5f);
        maze.transform.position = position;
        maze.transform.eulerAngles = rotation;
        maze.transform.localScale = RescaleToScaleFactor(maze);
        if (maze != null)
        {
            AddMeshColliderToAllChildren(maze);
        }
        //Debug.Log("ObjectCollectionManager : CreateMaze() : maze final location = " + maze.transform.position);

        StartMaze.Instance.MazeIsGenerated();
    }

    private void AddMeshColliderToAllChildren(GameObject obj)   //Function to add mesh collider to every childs of a gameobject
    {
        for (int i = 0; i < obj.transform.childCount; i++)
        {
            obj.transform.GetChild(i).gameObject.AddComponent<MeshCollider>();
        }
    }

    private Vector3 RescaleToScaleFactor(GameObject objectToScale)  //Rescale the object depending on the scale factor
    {
        return objectToScale.transform.localScale * ScaleFactor;
    }
}
