﻿using HoloToolkit.Unity;
using UnityEngine;
using System;
using System.IO;
using System.Text;

public class CameraMovementTracker : Singleton<CameraMovementTracker>
{
    //[SerializeField]
    private bool _isTracking = false;  //Is the camera being tracked ?
    private readonly float _sampleTime = 0.03f;  //Time between each sample

    private Vector3 _lastSampleLocation;
    private Vector3 _lastSampleRotation;
    private float _lastSampleTime;
   
    string LineToWrite;
    string FilePath; //Path where the logs file will be saved

    const string SEP = ";"; //Define the separator between each data for the CSV file

        
    public void SetTrackingCameraMovementTracker(bool setTracking){ //Function to activate the tracker and start generating logs
        _isTracking = setTracking;
        if(_isTracking)
            SpatialUnderstandingState.Instance.SpaceQueryDescription = "Generating LOGS...";
    }

    void Start()
    {
        StringBuilder csvcontent = new StringBuilder();
        string filename = DateTime.Now.ToString("yyyy-MM-dd--HH-mm-ss") + ".csv";  //The logs file will be named as the current date
        FilePath = Path.Combine(Application.persistentDataPath, filename);
        _lastSampleTime = Time.time;    //Get the current time
        Debug.Log("New log-file created : " + filename);
        LineToWrite = "Time" + SEP + "X" + SEP + "Y" + SEP + "Z" + SEP + "Xrot" + SEP + "Yrot" + SEP + "Zrot";
        csvcontent.AppendLine(LineToWrite); //Add the end-of-line character to the line
        File.AppendAllText(FilePath, csvcontent.ToString());
    }

    void Update()
    {
        if(_isTracking)
        {
            if (Time.time - _lastSampleTime > _sampleTime) //Check if it is time to get a new sample
            {
                StringBuilder csvcontent = new StringBuilder();
                _lastSampleTime = Time.time;    //Get the current time to compare it with last time sample
                _lastSampleLocation = CameraCache.Main.transform.position;  //Get the position (x, y, z) of the camera (= user's position)
                _lastSampleRotation = CameraCache.Main.transform.rotation.eulerAngles;  //Get the orientation of the user (where he is facing) as Euler angles
                LineToWrite = _lastSampleTime.ToString("G4") + SEP + _lastSampleLocation.x.ToString("G4") + SEP + _lastSampleLocation.y.ToString("G4") + SEP + _lastSampleLocation.z.ToString("G4") + SEP + _lastSampleRotation.x.ToString("G4") + SEP + _lastSampleRotation.y.ToString("G4") + SEP + _lastSampleRotation.z.ToString("G4"); //Concatenate all the datas as a string
                csvcontent.AppendLine(LineToWrite); //Add the end-of-line character to the line
                File.AppendAllText(FilePath, csvcontent.ToString());    //Create the file if it doesnt exist, and add the line at the end of the CSV file 
            }
        }
    }
}
