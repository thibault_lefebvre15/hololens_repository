﻿using System;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity;

//This script is used to generate a maze specified by a CSV file

public class CreateMazeFromCSV : Singleton<CreateMazeFromCSV>
{
    private int NbWalls = 0;    //Number of walls composing the maze
    private List<float> xValues = new List<float>();    //List of the x coordinates for each wall of the maze
    private List<float> zValues = new List<float>();    //List of the z coordinates for each wall of the maze
    private List<float> lengthValues = new List<float>();   //List of the lengths for each wall of the maze
    private List<float> rotationValues = new List<float>(); //List of the rotations for each wall of the maze : 0 for a wall perpendicular to the user's look, 90 for parallel

    private float Xmin = 1000f; //Minimum x coordinate of the maze
    private float Zmin = 1000f; //Minimum z coordinate of the maze
    private float Xmax = 0f;    //Maximum x coordinate of the maze
    private float Zmax = 0f;    //Maximum z coordinate of the maze

    const float WALL_WIDTH = 0.1f;  //wall width in meters
    const float WALL_HEIGHT = 2.0f; //wall height in meters
    const float FLOOR_WIDTH = 0.02f;    //Floor width in meters
    const float POLE_RADIUS = 0.2f; //Radius of the pole that indicate the bottom left corner of the maze

    public Vector3 MazeSize = new Vector3();    //Contains the final size of the maze in the 3 dimensions

    private void ReadCSV()  //Funtion to read the CSV file and store data in the lists
    {
        TextAsset MazeCSV = Resources.Load("maze") as TextAsset;    //Load the CSV file
        var lines = MazeCSV.text.Split('\n'); //Separate the file's lines
        float temp;
        foreach (var line in lines) //Read every lines of the file
        {
            var values = line.Split(';');
            if (values.Length == 4 && values[0] != "X") // Check if the line being read has four fields as expected and if it's not the header
            {
                NbWalls++;  //Count the number of walls described by the CSV file
                float.TryParse(values[0], out temp); //x coordinate of the left side of the wall
                xValues.Add(temp);
                float.TryParse(values[1], out temp); //z coordinate of the bottom side of the wall (the side closest to the user)
                zValues.Add(temp);
                float.TryParse(values[2], out temp); //Length of the wall
                lengthValues.Add(temp);
                float.TryParse(values[3], out temp); //Orientation of the wall : 0 or 90 as describbed earlier
                rotationValues.Add(temp);
            }
        }
    }

    private void FindMaxMinCoords() //This function is used to find the maximum and minimum values for x and z
    {
        if (xValues.Count == 0 || zValues.Count == 0)
        {
            throw new InvalidOperationException("Empty list");
        }


        for (int i = 0; i < NbWalls; i++) //Loop through all elements of both xValues and zValues lists to look for the biggest and smallest ones. We cant use foreach as the lists' elements have to be modified
        {
            if (rotationValues[i] == 0) //If the wall is perpendicular to the user, the xmax value has to be calculated with the left side (given in the CSV file) plus the length
            {
                if (xValues[i] + lengthValues[i] > Xmax)
                {
                    Xmax = xValues[i] + lengthValues[i];
                }
                if (xValues[i] < Xmin)
                {
                    Xmin = xValues[i];
                }
                if (zValues[i] > Zmax)
                {
                    Zmax = zValues[i];
                }
                if (zValues[i] < Zmin)
                {
                    Zmin = zValues[i];
                }
            }

            else if (rotationValues[i] == 90)   //If the wall is parallel to the user, the zmax value has to be calculated with the bottom side (given in the CSV file) plus the length
            {
                if (xValues[i] + lengthValues[i] > Xmax)
                {
                    Xmax = xValues[i];
                }
                if (xValues[i] < Xmin)
                {
                    Xmin = xValues[i];
                }
                if (zValues[i] + lengthValues[i] > Zmax)
                {
                    Zmax = zValues[i] + lengthValues[i];
                }
                if (zValues[i] < Zmin)
                {
                    Zmin = zValues[i];
                }
            }
        }
    }


    private void CenterMaze() //Move the maze so that its bottom-left corner is the origin
    {
        for (int i = 0; i < NbWalls; i++) //Loop through all elements of both xValues and zValues lists. We cant use foreach as the lists elements have to be modified
        {
            xValues[i] = xValues[i] - Xmin; //the position of each point become relative to the origin
            zValues[i] = zValues[i] - Zmin;
        }
        Xmax = MazeSize.x; //Update new min and max coords
        Zmax = MazeSize.z;
        Xmin = 0;
        Zmin = 0;
        //Debug.Log("Xmax and Zmax after maze being centered" + Xmax + "  " + Zmax);
    }

    private void CalculateMazeSize() //Used to calculate the maze size and store it into a Vector3
    {
        MazeSize.x = Xmax - Xmin;
        MazeSize.z = Zmax - Zmin;
        MazeSize.y = WALL_HEIGHT;
    }

    private void GenerateWalls()    //Generate all the walls of the maze
    {
        GameObject maze = GameObject.Find("Maze");  //Find the gameobject Maze that should be created directly in the unity scene
        Material brickWall = Resources.Load("Materials/WallTexture", typeof(Material)) as Material; //Load the material to apply on the walls
        for (int i = 0; i < NbWalls; i++)   //Loop for each wall that has to be built
        {
            GameObject wall = GameObject.CreatePrimitive(PrimitiveType.Cube);   //Create a cube that will be shaped to create a wall
            if (rotationValues[i] == 0) //Wall must be placed perpendicularly
            {
                wall.name = "wall" + i;
                wall.transform.position = new Vector3(xValues[i] + (lengthValues[i]) / 2, WALL_HEIGHT / 2, zValues[i]); // Positionning the initial cube so that it is the center of the futur wall
                wall.transform.localScale = new Vector3(lengthValues[i], WALL_HEIGHT, WALL_WIDTH);   // Extend the cube depending on the wall caracteristics
                wall.GetComponent<Renderer>().material = brickWall; //Set the wall texture
                wall.transform.parent = maze.transform; //Set the maze as the wall's parent
            }
            else if (rotationValues[i] == 90) //Wall must be placed parallel
            {
                wall.name = "wall" + i;
                wall.transform.position = new Vector3(xValues[i], WALL_HEIGHT / 2, zValues[i] + (lengthValues[i]) / 2); // Positionning the initial cube so that it is the center of the futur wall
                wall.transform.localScale = new Vector3(WALL_WIDTH, WALL_HEIGHT, lengthValues[i]);   // Extend the cube depending on the wall caracteristics
                wall.GetComponent<Renderer>().material = brickWall; //Set the wall texture
                wall.transform.parent = maze.transform; //Set the maze as the wall's parent
            }
        }
    }


    private void PoleAtBottomLeftCorner() //Create a pole at the bottom left corner as a landmark to help placing maze when using tap to place
    {
        GameObject maze = GameObject.Find("Maze");  //Find the gameobject Maze that should be created directly in the unity scene
        GameObject Pole = GameObject.CreatePrimitive(PrimitiveType.Cylinder);   //Create a cylinder that will be shaped to create the pole
        Pole.name = "BottomLeftCorner";
        Pole.transform.position = new Vector3(Xmin, WALL_HEIGHT / 2, Zmin); //Set the pole position as the bottom left corner of the maze
        Pole.transform.localScale = new Vector3(POLE_RADIUS, WALL_HEIGHT / 2, POLE_RADIUS); //Extend the pole to have the same height as the walls
        Pole.transform.parent = maze.transform; //Set the maze as the pole's parent
    }

    private void GenerateFloor()    //Create the floor under the walls
    {
        GameObject maze = GameObject.Find("Maze");  //Find the gameobject Maze that should be created directly in the unity scene
        Material GreenFloor = Resources.Load("greenfloor", typeof(Material)) as Material; //Load the material to apply on the floor
        GameObject floor = GameObject.CreatePrimitive(PrimitiveType.Cube);   // Extend the cube depending on the floor caracteristics
        floor.name = "Floor";
        floor.transform.position = new Vector3(MazeSize.x / 2, FLOOR_WIDTH / 2, MazeSize.z / 2);
        floor.transform.localScale = new Vector3(MazeSize.x, FLOOR_WIDTH, MazeSize.z);
        floor.GetComponent<Renderer>().material = GreenFloor;
        floor.transform.parent = maze.transform;
    }

    public void CreateMaze()    //Function that has to be called to create the whole maze
    {
        if (GameObject.Find("Maze") == null)    //Check if the gameobject Maze has been properly created in the Unity project
        {
            Debug.Log("The gameobject Maze doesn't exist in the Unity project, please create it");
            return;
        }
        ReadCSV();
        FindMaxMinCoords();
        CalculateMazeSize();
        CenterMaze();
        GenerateWalls();
        GenerateFloor();
        PoleAtBottomLeftCorner();
    }
}