using HoloToolkit.Unity;
using UnityEngine;

public class StartMaze : Singleton<StartMaze>
{
    public bool isMazeGenerated= false;
    public bool isTapToPlace = false;
    public bool isReadyToStart = false;
    public bool isMazeStarted = false;
    
    public void EndTapToPlace()
    {
        isTapToPlace = false;
        isReadyToStart = true;

        GameObject.Find("Maze").GetComponent<HoloToolkit.Unity.SpatialMapping.TapToPlace>().enabled = false;

        Debug.Log("StartMaze: Tap to place enable: " + GameObject.Find("Maze").GetComponent<HoloToolkit.Unity.SpatialMapping.TapToPlace>().enabled);



        SpatialUnderstandingState.Instance.SpaceQueryDescription = "Ready, tap to start";
    }

    public void EnableTapToPlace()
    {
        isTapToPlace = true;

        GameObject.Find("Maze").GetComponent<HoloToolkit.Unity.SpatialMapping.TapToPlace>().enabled = true;
        Debug.Log("StartMaze: Tap to place enable: " + GameObject.Find("Maze").GetComponent<HoloToolkit.Unity.SpatialMapping.TapToPlace>().enabled);


        SpatialUnderstandingState.Instance.SpaceQueryDescription = "Tap to Place on a better location";
    }

    public void StartRecordMaze()
    {
        isMazeStarted = true;
        isReadyToStart = false;

        CameraMovementTracker.Instance.SetTrackingCameraMovementTracker(true);
        

        SpatialUnderstandingState.Instance.HideText = true;
        SpatialUnderstandingState.Instance.SpaceQueryDescription = "Lauch Complete"; // Never displayed (text hided)

        Debug.Log("StartMaze: Start Recording logs");
    }

    public void MazeIsGenerated()
    {
        isMazeGenerated = true;
        SpatialUnderstandingState.Instance.SpaceQueryDescription = "Maze Generated";

        Debug.Log("StartMaze: Maze Generated");
    }

}

