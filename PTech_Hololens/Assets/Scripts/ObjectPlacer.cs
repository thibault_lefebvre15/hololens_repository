﻿using System;
using System.Collections.Generic;
using HoloToolkit.Unity;
using UnityEngine;

public class ObjectPlacer : MonoBehaviour
{

    public SpatialUnderstandingCustomMesh SpatialUnderstandingMesh;
    public Material OccludedMaterial;
    private SpatialUnderstandingDllTopology.TopologyResult[] _resultsFloorTopology = new SpatialUnderstandingDllTopology.TopologyResult[1];
    private SpatialUnderstandingDllTopology.TopologyResult _resultWallTopology;
    private bool _timeToHideMesh;




    void Start()  {}

    void Update()
    {
        if (_timeToHideMesh)    //When the spatial mapping is done, the mesh has to be hidden once
        {
            HideGridEnableOcclusion();
            _timeToHideMesh = false;
            Debug.Log("ObjectPlacer : Update() : _timeToHideMesh passe de true à " + _timeToHideMesh);
        }
    }

    private void HideGridEnableOcclusion()  //Change the spatial understanding mesh material so that it is occluded by reality
    {
        SpatialUnderstandingMesh.MeshMaterial = OccludedMaterial;
        //Debug.Log("ObjectPlacer : HideGridEnableOcclusion()");
    }





    public void CreateScene()   //Function that has to be called to initiate the creation of the scene i.e. create the maze, find the proper position to place it, and place it
    {
        if (!SpatialUnderstanding.Instance.AllowSpatialUnderstanding)   // Only if we're enabled
        {
            Debug.Log("ObjectPlacer : CreateScene() : SpatialUnderstanding non aurotorise");
            return;
        }
        SpatialUnderstandingState.Instance.SpaceQueryDescription = "Generating World"; //Update text
        _timeToHideMesh = true;
        PlaceMazeOnFloor();
    }

    private void PlaceMazeOnFloor() //Finds the position of the center of the largest floor detected by the spatial mapping, and the facing direction of the largest wall
    {
        var resultsFloorTopologyPtr = SpatialUnderstanding.Instance.UnderstandingDLL.PinObject(_resultsFloorTopology);
        var resultsWallTopologyPtr = SpatialUnderstanding.Instance.UnderstandingDLL.PinObject(_resultWallTopology);
        SpatialUnderstandingDllTopology.QueryTopology_FindLargestPositionsOnFloor(_resultsFloorTopology.Length, resultsFloorTopologyPtr);
        SpatialUnderstandingDllTopology.QueryTopology_FindLargestWall(resultsWallTopologyPtr);
        ObjectManager.Instance.PlaceMaze(_resultsFloorTopology[0].position, _resultWallTopology.normal); //Initiate the maze placing at the coordinates and rotation determinated
    }
}